/*
 * Using seq_file to print out a tree to a proc file
 *
 * Samman Bikram Thapa: Coding and Documentation
 * Saurav Keshari Aryal: Coding and Documentation
 *
 * We got a lot of help from Ram Hari Dahal. A lot of the code might look similar
 * but all the work done here is done without the involvement of direct 
 * copy and pasting of code. Since we only have very few logic to implement
 * and are limited to use specific methods, the code base for his project implementation
 * and our implementation shows similarity.
 *
 * Code derived from example/explanation located at: 
 *   http://lwn.net/Articles/22355/
 *   https://www.kernel.org/doc/Documentation/filesystems/seq_file.txt
 * For more about kernel red-black trees, see:
 * 	http://lwn.net/Articles/184495/
*/

/* DONE:
*	Fill in implementation where DONE tags exist
 * 	Complete seq_file implementation
 * 	Add parametrization to control size of the tree
 * 	Add locking and also identify critical section
 */

#include <linux/init.h>
#include <linux/module.h>
#include <linux/proc_fs.h>
#include <linux/fs.h>
#include <linux/seq_file.h>
#include <linux/slab.h>
#include <linux/rbtree.h>
#include <linux/errno.h>

MODULE_AUTHOR("Samman(samman.thapa@bison.howard.edu), Saurav(saurav.aryal@bison.howard.edu), Prajjwal Dangal (prajjwal.dangal@bison.howard.edu)");
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Creates a /proc entry, which upon reading displays the module's red black tree");

/* DONE: This value should be turned into a parameter */
int NUM_ITEMS = 100;
module_param(NUM_ITEMS, int, S_IRUSR);
MODULE_PARM_DESC(NUM_ITEMS, "Number of Items");
// #define NUM_ITEMS 100

/* pointer to root node, so that module_exit can clean up the tree */
static struct rb_root *tree_root = NULL;

/* data to store in red-black tree */
struct all_data {

	/* rb_node is an index in the rbtree */
	struct rb_node rb_node;

	unsigned int rb_key;
  
	unsigned int some_data;

};

/*
 * adds a node to rbtree
 *
 * @root: root of the rbtree
 * @new: node to insert
 *
 */
static void my_rb_insert( struct rb_root *root, struct all_data *new ) 
{	
	struct rb_node **link; /* link of some struct all_data in tree */
	struct rb_node *parent = NULL;
	unsigned int key = 0;
	struct all_data *next;

	if(!new || !root)
		return;

	link = &root->rb_node;
	key = new->rb_key;

	/* Find the bottom of the tree where new node will be inserted */
	while (*link) {
		/* get struct all_data for current location in tree */
		parent = *link;
		next = rb_entry(parent, struct all_data, rb_node);

		/* descend left or right based on keys */
		if (next->rb_key > key)
			link = &parent->rb_left; 
		else
			link = &parent->rb_right; 
	}

	/* add the node. link points to parent's rb_left or rb_right pointer */
	rb_link_node(&new->rb_node, parent, link);
	rb_insert_color(&new->rb_node, root);
}

/* 
 * Creates a rb tree of some useless data for illustrative purposes.
 * This data will be present in the proc file created by this module.
 *
 * Called on module init
 *
 * @num_items: the number of entries to create
 *
 */
static int construct_tree( int num_items ) 
{
	int i;
	struct all_data *some_item;
	if( !tree_root )
		return -EFAULT;

	/* this implementation keys data inversely to the data value, 
	 * i.e. data will be sorted backwards */
	for(i = 0; i < num_items; i++ ) {
		/* allocate new struct all_data */
		some_item = kmalloc(sizeof(struct all_data), GFP_KERNEL);
		if(!some_item)
			return -ENOMEM;
		
		/* set key and data */
		some_item->rb_key = num_items - i;
		some_item->some_data = i;
		
		/* add to tree */
		my_rb_insert( tree_root, some_item );
	}

	return 0;
}




/* seq_file code */

/*
 * The sequence iterator functions.  The iterator is a position in 
 * the rbtree.
 *
 * @s: The seq file from open
 * @pos: offset to a position within the seq file
 */
static void *tt_seq_start(struct seq_file *s, loff_t *pos) 
{	
	struct rb_node *spos = NULL;
	
	/* DONE: locking? */
	spin_lock(&spos);
	
	/* DONE: set spos to the first node in the tree */
	spos = rb_first(tree_root);
	

	if (! spos)
		return NULL;

	/* 
	 * DONE: pos is the logical offset to begin reading, 
	 * so spos should be advanced until it reaches the offset 
	 * specified by pos. 
	 * Thus, pos == 0 should start reading at the 
	 * start of the file. Also, seq_start is called after seq_stop, 
	 * so seq_start should identify when the file is 'done' being read
	 * (probably by using pos?)
	 *
	 */
	seq_printf(s, " Reading proc file: %lu\n", *pos);

	/* print a header for the file */
	seq_printf(s, "Key\tValue\n");

	return spos;
}

/*
 * This function advances the iteration from v to the next iterator in the 
 * data structure.
 *
 * @s: the seq_file that is written to
 * @v: the pointer to the current iterator (an rb_node*)
 * @pos: a logical offset in the file being read
 *
 * Returns a pointer to the next iterator
 *
 */
static void *tt_seq_next(struct seq_file *s, void *v, loff_t *pos) 
{
	struct rb_node *spos = NULL;

	/* DONE: Get the node from the iterator and store it in spos */
	spos = (struct rb_node *) v;
	
	/* DONE: Retrieve the next iterator
	 *	This may warrant locking?
	 */
	spos = (void *) rb_next(spos);


	/* increment the logical offset of the file */
	++*pos;

	/* return the next iterator */
	return spos;
}

/* 
 * DONE: Called when sequence is finished reading (i.e. next() returns NULL)
 * Should clean up any allocations or locking that is still open.
 */
static void tt_seq_stop(struct seq_file *s, void *v) 
{
	struct rb_node *spos = (struct rb_node *) v;
    spin_unlock(&spos);
    kfree(v);
}

/*
 * The show function.  Prints the data available from the iterator to the seq file.
 *
 * @s: the seq file
 * @v: the iterator (an rb_node*)
 */
static int tt_seq_show(struct seq_file *s, void *v) 
{
	struct rb_node *spos;
	struct all_data* data;

	/* DONE: Get the node from the iterator and store it in spos */
	spos = (struct rb_node *) v;
	
	/* DONE: retrieve the data from spos */
    data = rb_entry(spos, struct all_data, rb_node);

	/* Print it out */
	seq_printf(s, "%u\t%u\n", data->rb_key, data->some_data);

	return 0;
}

/*
 * Tie the seq functions together into a set of seq_operations.
 */
static struct seq_operations tt_seq_ops = 
{

	/* DONE: map the functions to the standard seq_operations */
	.start = tt_seq_start,
    .next = tt_seq_next,
    .stop = tt_seq_stop,
    .show = tt_seq_show
};


/*
 * Set up the file operations for our /proc file.  In this case,
 * all we need is an open function which sets up the sequence ops.
 */
static int tt_open(struct inode *inode, struct file *file) 
{
	return seq_open(file, &tt_seq_ops);
};

/*
 * The file operations structure contains our open function along with
 * set of the canned seq_ops.
 */
static struct file_operations tt_file_ops = {
	.owner   = THIS_MODULE,
	.open    = tt_open,
	.read    = seq_read,
	.llseek  = seq_lseek,
	.release = seq_release
};


/*
 * Module setup
 *
 * Creates the tree and proc file
 *
 */
static int tt_init(void) 
{ 	  
	struct proc_dir_entry *entry;


	/* create and initialize a new red-black tree */
	tree_root = kmalloc(sizeof(struct rb_root),GFP_KERNEL);
	if(!tree_root)
		return -ENOMEM;
	*tree_root = RB_ROOT;

	/* build the tree */
	/* DONE: Parametrize NUM_ITEMS 
	  * Check for errors. 
	  */
	construct_tree( NUM_ITEMS );

	/* DONE: Open the proc file and bind the file operations */
	create = proc_create("tree_traversal", 0, NULL, &tt_file_ops);
	
	return 0;
}

/* 
 * Module removal
 *
 * Needs to clean up exported tree and remove the proc file
 *
 */
static void tt_exit(void) 
{
	struct rb_node *node_iter;
	struct rb_node *next_node_ptr;
	struct all_data *data;

	
	/* DONE: free the nodes and data in the tree. make a helper function? */
    while(node_iter) {
        next_node_ptr = rb_next(node_iter);
        data = rb_entry(node_iter, struct all_data, rb_node);
        kfree(data);
        kfree(node_iter);
        node_iter = next_node_ptr;
    }

	
	/* tree should be empty now */
	kfree(tree_root);
	
	/* DONE: delete the proc file */
	remove_proc_entry("tree_traversal", NULL);

}

module_init(tt_init);
module_exit(tt_exit);

